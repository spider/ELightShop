﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="photoupload.aspx.cs" Inherits="_manage_upload_photoupload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>图片上传</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
*{padding: 0px; margin: 0px;}
html,body{font-size: 12px; color: #333333; text-align: left; line-height: 18px; margin:0px auto; background: #FFFFFF;}
#divUpload #filPhoto{padding:2px;}
#divUpload #btnUpload{height:20px;}
</style>
</head>
<body>
<form id="photoUpload" name="photoUpload" enctype="multipart/form-data" runat="server" action="PhotoUpload.aspx" method="post">
    <div id="divUpload">
        <input id="filPhoto" name="filPhoto" type="file" runat="server" size="20" />
        <input id="btnUpload" name="btnUpload" type="submit" value="上传" />
        <input id="hidepic" name="hidepic" type="hidden" value="" runat="server" />
		<input id="hideurl" name="hideurl" type="hidden" value="" runat="server" />
    </div>
</form>
</body>
</html>
