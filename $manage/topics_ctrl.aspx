﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="topics_ctrl.aspx.cs" Inherits="_manage_topics_ctrl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Include/Validator.js"></script>
<script type="text/javascript" src="Include/textLimitChk.js"></script>
</head>
<body>
<form id="xform" name="xform" runat="server" action="" method="post" onSubmit="return Validator.Validate(this,3);">
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
  <tr>
    <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#"><%=SecPName%></a></td>
  </tr>
</table>
<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
  <tr>
    <td align="center"><h2><%=MainPName%>管理中心----<%=SecPName%></h2></td>
  </tr>
</table>
  <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1">
    <tbody>
      <tr>
        <td class="BarStyleV1"><h3>管理 </h3></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="BarStyleV1"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1" style="margin-top: 0px; border: none;">
            <tbody>
              <tr>
                <td width="100" align="right" valign="middle">名称：</td>
                <td align="left" valign="middle"><input id="titler" maxlength="120" size="35" name="titler" msg="" datatype="Require" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">缩略图：</td>
                <td align="left" valign="middle"><iframe allowtransparency="true" id="PhotoUpload" name="PhotoUpload" runat="server" frameborder="0" width="290" scrolling="no" height="22"></iframe>
                  <br />
                  <span class="Tip_Mess">* 上传图片大小 475 X 164 dpi</span>
                  <input name="img" type="hidden" id="img" value="" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">摘要：</td>
                <td align="left" valign="middle"><textarea id="sim" name="sim" rows="6" cols="56" onkeyup="Limitchk(this,70,250)" runat="server"></textarea></td>
              </tr>
              <tr>
                <td align="right" valign="middle">产品：</td>
                <td align="left" valign="middle"><textarea id="proid" name="proid" rows="6" cols="56" onkeyup="Limitchk(this,70,250)" runat="server"></textarea></td>
              </tr>
              <tr>
                <td align="right" valign="middle">模版：</td>
                <td align="left" valign="middle"><textarea id="templ" name="templ" rows="6" cols="56" runat="server"></textarea></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="center">
                    <input type="submit" value="提交" name="Submit" class="Btn_v1" id="Submit" />
                    <input id="action" type="hidden" value="ctrl_add" name="action" runat="server" />
                    <input id="actid" type="hidden" value="0" name="actid" runat="server" />
                    <input type="reset" value="重置" name="reset" class="Btn_v1" />
                </td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</form>
</body>
</html>
