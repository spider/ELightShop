﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

using YduCLB.SQL;
using YduCLB.Func;


public partial class _manage_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region
        if (IsPostBack)
        {
            if (code.Value != Session["CheckCode"].ToString().ToLower())
            {
                Other.net_alert("验证码错误！" + Session["CheckCode"].ToString().ToLower(), "login.aspx");
            }
            if (name.Value == string.Empty || pass.Value == string.Empty)
            {
                Other.net_alert("用户名或密码不能为空！", "login.aspx");
            }

            string md5_pass = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(pass.Value, "MD5").ToLower().Substring(8, 16);
            Conn Tc = new Conn();
            SqlDataReader objDataReader = Tc.ConnDate("select * from " + TableName.db_admin + " where username='" + name.Value + "' and passwd='" + md5_pass + "'");
            if (objDataReader.Read())
            {
                Session["Admin"] = objDataReader["username"].ToString();
                Session["AdminID"] = objDataReader["id"].ToString();
                objDataReader.Close();
                SqlDataReader objUpData = Tc.ConnDate("update " + TableName.db_admin + " set login_time='" + DateTime.Now + "'");
                Response.Redirect(Si.siteurl+"$manage/manage.aspx");
				objUpData.Dispose();
            }
            else
            {
                Response.Redirect(Si.siteurl+"$manage/login.aspx");
          

            }
            objDataReader.Dispose();
			Tc.CloseConn();
        }
        #endregion
    }
}
