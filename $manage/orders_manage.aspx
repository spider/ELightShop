﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="orders_manage.aspx.cs" Inherits="_manage_orders_manage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" language="javascript">
function ustatic(elem,num){
	elem.src="common_ustatic.aspx?uid="+num+"&lib=1";
}
</script>
</head>
<body>
<form id="form1" runat="server">
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
  <tr>
    <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#">管理<%=SecPName%></a></td>
  </tr>
</table>
<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
  <tr>
    <td align="center"><h2><%=MainPName%>管理中心----管理<%=SecPName%></h2></td>
  </tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" class="TableSt1">
  <tr>
    <td height="25" align="left" valign="middle"><h3>管理 </h3></td>
    <td width="18%" align="center" valign="middle"></td>
  </tr>
  <tr>
    <td height="25" colspan="2" align="right" valign="middle"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="8%" align="center" valign="middle">ID序号</td>
          <td width="" align="center" valign="middle">订单号</td>
          <td width="13%" align="center" valign="middle">产品</td>
          <td width="8%" align="center" valign="middle">账户</td>
          <td width="18%" align="center" valign="middle">订单时间</td>
		  <td width="10%" align="center" valign="middle">管理</td>
        </tr>
        <tr>
          <td height="25" colspan="7" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="TrHover">
          <asp:Repeater id="Ft_List" runat="server">
            <ItemTemplate>
                <tr>
                    <td width="8%" align="center" valign="middle"><%#Eval("id")%></td>
                    <td width="" align="center" valign="middle"><%#Eval("orderno")%></td>
					<td width="13%" align="center" valign="middle">&nbsp;</td>
                    <td width="8%" align="center" valign="middle"><%#Eval("memid")%></td>
                    <td width="18%" align="center" valign="middle"><%#Eval("relea_time")%></td>
                    <td width="10%" align="center" valign="middle"><a href="javascript:void(0)" onClick="window.open('<%=EventName%>_ctrl.aspx?act=1&id=<%#Eval("id")%>','editartframe');">详细</a> | <a href="all_del.aspx" onClick="javascript:return confirm('提示：您确定要继续执行该操作吗？')">删除</a></td>
                </tr>
            </ItemTemplate>
          </asp:Repeater>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="40" colspan="2" align="right" valign="middle"><a href="?page=1">首页</a>&nbsp; <a href="?page=<%=CurPage-1%>">上一页</a> &nbsp; <a href="?page=<%=CurPage+1%>">下一页</a>&nbsp; <a href="?page=<%=PageCount%>">尾页</a> &nbsp; 页次：<%=CurPage%>/<%=PageCount%>页</td>
  </tr>
</table>
</form>
</body>
</html>
