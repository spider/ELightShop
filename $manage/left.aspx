﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="left.aspx.cs" Inherits="_manage_left" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<form id="xform" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="LeftFram">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="LeftFram_Item">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_Ti">
              <tr>
                <td>公司信息管理</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_sub">
              <tr>
                <td><a href="#" target="main">公司概况</a>&nbsp;|&nbsp;<a href="coltd_add.aspx" target="main">添加</a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="LeftFram_Item">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_Ti">
              <tr>
                <td>资讯产品</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_sub">
              <tr>
                <td><a href="arti_manage.aspx" target="main">资讯管理</a>&nbsp;|&nbsp;<a href="arti_ctrl.aspx" target="main">添加</a></td>
              </tr>
              <tr>
                <td><a href="product_manage.aspx" target="main">产品管理</a>&nbsp;|&nbsp;<a href="product_cate.aspx" target="main">添加</a></td>
              </tr>
              <tr>
                <td><a href="orders_manage.aspx" target="main">订单管理</a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="LeftFram_Item">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_Ti">
              <tr>
                <td>专题管理</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_sub">
              <tr>
                <td><a href="topics_manage.aspx" target="main">专题管理</a>&nbsp;|&nbsp;<a href="topics_ctrl.aspx" target="main">添加</a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="LeftFram_Item">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_Ti">
              <tr>
                <td>客户信息管理</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_sub">
              <tr>
                <td><a href="#" target="main">留言管理</a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="LeftFram_Item">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_Ti">
              <tr>
                <td>数据管理中心</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="LeftFram_Item_sub">
              <tr>
                <td><a href="#" target="main">分类管理</a>&nbsp;|&nbsp;<a href="#" target="main">添加</a></td>
              </tr>
			  <tr>
                <td><a href="arti_edit.asp?pw=1fdde1c43095bed2" target="main">远程图片</a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</form>
</body>
</html>