﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;
using YduCLB.Func;

public partial class _manage_topics_ctrl : System.Web.UI.Page
{
    public string EventName = "topics";
    public string MainPName = "专题";
    public string SecPName = "添加专题";
    public string UpFileUrl = "co_arti/";
    Conn Tc = new Conn("level0");
    DbCtrl Dc = new DbCtrl();

    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();

        PhotoUpload.Attributes["src"] = "upload/photoupload.aspx?pic=img&intent=" + UpFileUrl;

        int nCtrl = Convert.ToInt32(Request.QueryString["act"]);
        int nId = Convert.ToInt32(Request.QueryString["id"]);

        if (nCtrl == 1)
        {
            SecPName = "编辑专题";
            action.Value = "ctrl_edit";

            SqlDataReader drReaderEd = Tc.ConnDate("select * from " + TableName.db_topics + " where id=" + nId);
            while (drReaderEd.Read())
            {
                actid.Value = drReaderEd["id"].ToString();
                titler.Value = drReaderEd["title"].ToString();
                img.Value = drReaderEd["small_pic"].ToString();
                sim.Value = drReaderEd["brief"].ToString();
                proid.Value = drReaderEd["selid"].ToString();
                templ.Value = drReaderEd["templ"].ToString();
            }
            drReaderEd.Dispose();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region
        if (IsPostBack)
        {
            Models Md = new Models();
            Other Ot = new Other();
            string appdir = string.Empty;
            appdir = Ot.GetAppSetValue("AppDir");

            string actid_ = actid.Value;
            string titler_ = titler.Value.Replace("\"", "&quot;");
            string img_ = img.Value;
            string sim_ = sim.Value;
            string proid_ = proid.Value;
            string templ_ = templ.Value;
            

            img_ = (img_ == "") ? "noimages.gif" : img_;
            //if (img_ != "noimages.gif")
            //{
            //    Ot.LimitImgW(img_, 300, "W");
            //}
            img_ = img_.Replace(appdir, "/");

            string[] fieldval = new string[]{"title:"+titler_,
                                                    "brief:"+sim_,
                                                    "small_pic:"+img_,
                                                    "selid:"+proid_,
                                                    "templ:"+templ_};
            int nResult = 1;
            if (action.Value == "ctrl_add")
            {
                nResult = Dc.AddRecord(TableName.db_topics, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", EventName + "_manage.aspx");
                }
            }
            if (action.Value == "ctrl_edit")
            {
                nResult = Dc.UpdateRecord(TableName.db_topics, "id=" + actid_, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", "close");
                }
            }


        }
        #endregion
		Tc.CloseConn();
    }
}
