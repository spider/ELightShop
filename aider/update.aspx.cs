﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using YduCLB.SQL;

public partial class aider_update : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();

        int nId=0;
        string sAct = string.Empty;
        string sStand = string.Empty;

        if (Request.QueryString["act"] != null)
        {
            nId = Convert.ToInt16(Request.QueryString["uid"]);
            sAct = Request.QueryString["act"];
            sStand = Request.QueryString["stand"];
        }

        if (Request.Form["act"] != null)
        {
            nId = Convert.ToInt16(Request.Form["uid"]);
            sAct = Request.Form["act"];
            sStand = Request.Form["stand"];
        }

        if (nId != 0 || sAct != "")
        {
            if (sAct == "add2wish")
            {
                if (Session["Member"] != null)
                {
                    try
                    {
                        if (sStand == "add")
                        {
                            SqlDataReader objDataReader = Tc.ConnDate("select id from " + TableName.db_wishlist + " where memid=" + Session["MemberID"].ToString() + " and proid=" + nId.ToString());
                            if (objDataReader.Read())
                            {
                                objDataReader.Close();
                                objDataReader.Dispose();
                                Response.Write("Exists");
                            }
                            else
                            {
                                objDataReader.Dispose();
                                SqlDataReader objUpData_001 = Tc.ConnDate("insert into " + TableName.db_wishlist + "(memid, proid) values(" + Session["MemberID"].ToString() + ", " + nId.ToString() + ")");
                                objUpData_001.Close();
                                objUpData_001.Dispose();
                                Response.Write("Finish");
                            }
                        }

                    }
                    catch
                    {
                        Response.Write("Error");
                    }
                }
                else
                {
                    Response.Write("NotSignIn");
                }
            }

            if (sAct == "currency")
            {
                sStand = sStand.Length == 3 ? sStand : "USD";
                SqlDataReader objUpData_002 = Tc.ConnDate("select id from " + TableName.db_rate + " where ident='" + sStand + "'");
                sStand = objUpData_002.Read() ? sStand : "USD";
                objUpData_002.Close();
                objUpData_002.Dispose();
                switch (sStand)
                {
                    case "USD":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    case "GBP":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    case "AUD":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    case "CAD":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    case "EUR":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    case "ZAR":
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = sStand;
                        break;
                    default:
                        System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = "USD";
                        break;
                }
                Response.Write("-_-");
            }

            if (sAct == "isearch")
            {
                string sTitle = string.Empty;
                
                SqlDataReader objUpData_003 = Tc.ConnDate("select top 6 id,static,title from " + TableName.db_product + " where title like '%" + sStand + "%' order by relea_time desc");
                try
                {
                    while (objUpData_003.Read())
                    {
                        sTitle = sTitle + "<li><a href=\"/store/" + objUpData_003["static"].ToString() + ".html\" title=\"" + objUpData_003["title"].ToString() + "\">" + Regex.Replace(objUpData_003["title"].ToString(), @"(" + sStand + ")", "<b>$1</b>", RegexOptions.IgnoreCase) + "</a></li>\n";
                    }
                }
                catch
                {

                }
                objUpData_003.Close();
                objUpData_003.Dispose();
                Response.Write(sTitle);
            }

            if (sAct == "cart_del!rec")
            {
                try
                {
                    if (sStand == "del")
                    {
                        SqlDataReader objUpData_004 = Tc.ConnDate("update " + TableName.db_cart + " set delmark=1 where id=" + nId);
                        objUpData_004.Close();
                        objUpData_004.Dispose();
                    }
                    if (sStand == "rec")
                    {
                        SqlDataReader objUpData_005 = Tc.ConnDate("update " + TableName.db_cart + " set delmark=0 where id=" + nId);
                        objUpData_005.Close();
                        objUpData_005.Dispose();
                    }
                }
                catch
                {

                }

                Response.Redirect("/stylecss/images/blank.gif");
            }

            if (sAct == "cart_cgnum")
            {
                try
                {
                    int nNum = 0;
                    nNum = Convert.ToInt16(sStand);
                    SqlDataReader objUpData_006 = Tc.ConnDate("update " + TableName.db_cart + " set amount=" + nNum + " where id=" + nId);
                    objUpData_006.Close();
                    objUpData_006.Dispose();
                }
                catch
                {

                }
                Response.Redirect("/stylecss/images/blank.gif");
            }
        }
        Tc.CloseConn();
    }
}
