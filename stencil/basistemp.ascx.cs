﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;

public partial class stencil_basistemp : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageCType = Page.GetType().ToString();
        
        //string PageTypeList;
        //PageTypeList = "|ASP.default_aspx|";
        //PageTypeList.IndexOf(PageCType);
        //System.Console.WriteLine(PageTypeList.IndexOf(PageCType));

        //加载CSS文件
        CtrlCss.Text = "<link href=\"/stylecss/master.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />";
        //加载JS文件
        CtrlJs.Text = "<script language=\"javascript\" type=\"text/javascript\" src=\"/jscript/$3.js\"></script>\n<script language=\"javascript\" type=\"text/javascript\" src=\"/jscript/comm.js\"></script>";
	
        CtrlJs.Text = CtrlJs.Text +"\n<script type=\"text/javascript\">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-34012732-1']);_gaq.push(['_trackPageview']);(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();</script>";

        string sCss = "\n<link href=\"/stylecss/{$css}.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />";
        string sJs = "\n<script language=\"javascript\" type=\"text/javascript\" src=\"/jscript/{$script}.js\"></script>";

        if (PageCType == "ASP.default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "index");
        }

        if (PageCType == "ASP.list_default_aspx" || PageCType == "ASP.search_default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "list");
        }

        if (PageCType == "ASP.store_default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "detail");
        }

        if (PageCType == "ASP.register_default_aspx" || PageCType == "ASP.signin_default_aspx" || PageCType == "ASP.infor_default_aspx" || PageCType == "ASP.infor_xhtml_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "simpage");
        }

        if (PageCType.IndexOf("ASP.account_")!=-1)
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "account");

            if (PageCType == "ASP.account_my_profile_aspx")
            {
                CtrlJs.Text = CtrlJs.Text + sJs.Replace("{$script}", "validator");
            }
        }

        if (PageCType == "ASP.cart_default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "cart");
        }

        if (PageCType == "ASP.news_default_aspx" || PageCType == "ASP.news_xhtml_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "news");
        }

        if (PageCType == "ASP.help_default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "help");
        }

        if (PageCType == "ASP.topics_default_aspx")
        {
            CtrlCss.Text = CtrlCss.Text + sCss.Replace("{$css}", "topic");
        }
    }
}
