﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.Front;
using YduCLB.SQL;


public partial class topics_default : System.Web.UI.Page
{
    public int CurPage, PageCount;
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        GList xGList = new GList();
        FCtrl xFctrl = new FCtrl();

        ArrayList Result = new ArrayList();
        string[] aRate = xFctrl.Cvrate();
        string sData = string.Empty;
        string sUid = string.Empty;
        string sTemp = string.Empty;
		string sPageTi = string.Empty;
        double nPrice = 0;


        try
        {
            SqlDataReader drReader = Tc.ConnDate("select * from " + TableName.db_topics + " where id=1");
            if (drReader.Read())
            {
                sPageTi = drReader["title"].ToString();
				sUid = drReader["selid"].ToString();
                sTemp = drReader["templ"].ToString();
            }
            drReader.Close();
            drReader.Dispose();

            string[] arrUid = sUid.Split(';');

            for (int i = 0; i < arrUid.Length; i++)
            {
                sData = string.Empty;
                Result = xGList.get_RecList(TableName.db_product, 1, 80, "id in(" + arrUid[i].ToString() + ")", new string[] { "id", "static", "title", "was", "price", "small_pic" });
                foreach (string[] aList in Result)
                {
                    nPrice = Convert.ToDouble(aList[3]) / Convert.ToDouble(aRate[0]);
                    aList[3] = aRate[1] + Math.Round(nPrice, 2).ToString();

                    nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
                    aList[4] = aRate[1] + Math.Round(nPrice, 2).ToString();

                    sData = sData + String.Format("<div class=\"momo\"><ul><li class=\"i1\"><a href=\"/store/{1}.html\"><img src=\"{5}\" alt=\"\" width=\"160\" height=\"160\" /></a></li><li class=\"i2\"><a href=\"/store/{1}.html\" title=\"{2}\">{2}</a></li><li class=\"i3\">{3}</li><li class=\"i4\">{4}</li><li class=\"i5\"><a href=\"/store/{1}.html\"></a></li></ul></div>", aList) + "\n";
                }
                sTemp = sTemp.Replace("{$" + (i + 1).ToString() + "}", sData);
            }
        }
        catch
        {

        }

        Ft_Topic.Text = sTemp;
		Ft_PageTitle.Text = sPageTi + " OfferTablets.com";
        Tc.CloseConn();
    }
}
