﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
function showtab(n){
	$3("#box_nhps div.dlist").each(function(){
		$3(this).style.display="none";
	});
	$3("#box_nhps ul.Ti_3 a").each(function(){
		$3(this).className="nor";
	});
	
	$3("#box_nhps div.dlist:"+n).style.display="block";
	$3.evtsrc().className="cur";
}
</script>
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="MainDiv">
    
    <div class="RightDiv">
    	<div class="obj_r_01"><a href="/store/ainol-novo-7-elf-7-inch-android-4-0-tablet-pc-with-1-5ghz-cortex-a9-dual-core-1gb-ram-hd-screen-8gb-rom-117.html"><img src="/stylecss/images/tem/1512.jpg" /></a></div>
    	<div class="box_nhps" id="box_nhps">
        	<ul class="Ti_3">
                <li class="i1"><h3><a class="cur" onmouseover="showtab(0);" href="javascript:void(0);" hidefocus="true">New Arrivals</a><a class="nor" onmouseover="showtab(1);" href="javascript:void(0);" hidefocus="true">Hot Sale</a></h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="dlist" id="dlist01" style="display: block;">
                <asp:Literal id="Ft_ProNew" runat="server"/>
                <p class="mclear"></p>
                <%--<div class="ctrl"><span class="pl">Prev</span><span class="pr">Next</span></div>--%>
            </div><!--dlist-->
            <div class="dlist" id="dlist02" style="display: none;">
                <asp:Literal id="Ft_ProHot" runat="server"/>
                <p class="mclear"></p>
                <%--<div class="ctrl"><span class="pl">Prev</span><span class="pr">Next</span></div>--%>
            </div><!--dlist-->
        </div>
    </div><!--RightDiv-->
    
    <div class="LeftDiv">
		<div class="box_nts">
        	<ul class="Ti_2">
                <li class="i1"><h3>New Topics</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <div class="mvessel">
                    <div class="mti"><h2><asp:Literal id="Ft_TopicTi" runat="server"/></h2></div>
                    <div class="mpic"><asp:Literal id="Ft_TopicPic" runat="server"/></div>
                    <div class="mtxt"><asp:Literal id="Ft_TopicBrief" runat="server"/></div>
                    <div class="mlink"><asp:Literal id="Ft_TopicLink" runat="server"/></div>
                </div>
            </div>
        </div><!--box_nts-->
    	<div class="obj_l_01"><a href="/store/teclast-p85-8-inch-android-4-0-ics-tablet-pc-with-capacitive-screen-allwinner-a10-1-5ghz-8gb-1gb-ddr3-2160p-camera-99.html"><img src="/uploadfiles/co_show/2012_05/20120521202456.jpg" alt="Teclast P85 8 Inch Android 4.0 ICS Tablet PC with Capacitive Screen,Allwinner A10 1.5GHz,8GB,1GB DDR3,2160P,Camera" /></a></div>
    	<div class="box_tse">
        	<ul class="Ti_2">
                <li class="i1"><h3>Top Sale Tablets</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <asp:Literal id="Ft_Tsale" runat="server"/>
            </div>
        </div><!--box_tse-->
    </div><!--LeftDiv-->
    
    <p class="mclear"></p>

</div><!--MainDiv-->
<Stencil:FootTemp id="FootTemp_" runat="server" isIndex="true" />
</body>
</html>