﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.Front;

public partial class about_default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GList xGList = new GList();
        Ft_News.Text = xGList.get_RecList(TableName.db_news,
                                    1,
                                    8,
                                    "Sort_id=1 order by Publi_time desc",
                                    "<li><a href=\"" + Si.siteurl + "infor/with-{0}.html\">{1}</a></li>",
                                    new string[] { "id", "Title:Left:15" },
                                    null);

        Ft_Help.Text = xGList.get_RecList(TableName.db_news,
                                    1,
                                    8,
                                    "Sort_id=3 order by Publi_time desc",
                                    "<li><a href=\"" + Si.siteurl + "help/with-{0}.html\">{1}</a></li>",
                                    new string[] { "id", "Title:Left:15" },
                                    null);
    }
}
