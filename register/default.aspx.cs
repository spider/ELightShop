﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class register_default : System.Web.UI.Page
{
    public bool regok=false;
    protected void Page_Load(object sender, EventArgs e)
    {
        string param = Request.QueryString["status"];
        regok = param == "S9668" ? true : false;
		Ft_PageTitle.Text = "Create an Account, OfferTablets.com";
        if (IsPostBack)
        {
            if (agree_term.Value == "true")
            {
                Conn Tc = new Conn();
                string nickname_ = nickname.Value;
                string emailadress_ = emailaddress.Value;
                string password_ = password.Value;
                string agree_email_ = agree_email.Value == "true" ? "1" : "0";
                try
                {
                    SqlDataReader objDataReader = Tc.ConnDate("select id from " + TableName.db_member + " where username=N'" + emailadress_ + "'");
                    if (objDataReader.Read())
                    {
                        Response.Redirect(Si.siteurl + "register/?status=E8982", false);
                        objDataReader.Dispose();
                    }
                    else
                    {   
                        objDataReader.Dispose();
                        SqlDataReader objUpData = Tc.ConnDate("insert into " + TableName.db_member + "(nickname,username,passwd,subscribe) values(N'" + nickname_ + "', N'" + emailadress_ + "', N'" + System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(password_, "MD5").ToLower().Substring(8, 16) + "', " + agree_email_ + ")");
                        Response.Redirect(Si.siteurl + "register/?status=S9668", false);
                        objUpData.Dispose();
                    }
                    
                }
                catch
                {
                    Response.Redirect(Si.siteurl + "register/?status=E8981");
                }
                Tc.CloseConn();
            }
        }
        if (!IsPostBack)
        {
            string sLalert = "<div><script language=\"javascript\" type=\"text/javascript\">iKits.light_alert(\"{$tips}\");</script></div>";
            string errors = Request.QueryString["status"];
            switch (errors)
            {
                case "E8981":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "Operation Error!");
                    break;
                case "E8982":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "The Email is exist, please enter another Email Address.");
                    break;
                default:
                    break;
            }
        }
    }
}
