﻿using System;

public class TableName
{
    public static string db_coltd = "atablet_coltd";
    public static string db_coltdcate = "atabletcoltd_cate";
    public static string db_arti = "atablet_arti";
    public static string db_articate = "atablet_articate";
    public static string db_product = "atablet_probasic";
    public static string db_procate = "atablet_procate";
    public static string db_proxpitem = "atablet_proxparamitem";
    public static string db_proxpvalue = "atablet_proxparamvalue";
    public static string db_topics = "atablet_topics";
    public static string db_cart = "atablet_cart";
    public static string db_orderbasic = "atablet_orderbasic";
    public static string db_order = "atablet_order";
    public static string db_wishlist = "atablet_wish";
    public static string db_rate = "atablet_erate";
    public static string db_mess = "atablet_mess";
    public static string db_admin = "atablet_admin";
    public static string db_member = "atablet_member";
    public static string db_membasic = "atablet_membasic";

	public TableName()
	{

	}
}
