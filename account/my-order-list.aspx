﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="my-order-list.aspx.cs" Inherits="account_my_order_list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="UserCen">
	<div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Account</a></div>
    <div class="RightDiv">
        <div class="acc_box">
            <div class="order_list" id="order_list">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_st02">
                <thead>
                    <tr>
                        <th align="center" style="height: 16px">Order No.</th>
                        <th width="12%" align="center" style="height: 16px">Date</th>
                        <th width="15%" align="center" style="height: 16px">Subtotal</th>
                        <th width="15%" align="center" style="height: 16px">Payment</th>
                        <%--<th width="12%" align="center" style="height: 16px">Shiping</th>--%>
                        <th width="13%" align="center" style="height: 16px">Order Status</th>
                        <th width="20%" align="center" style="height: 16px">Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <%--<tr>
                        <td align="center">945737748514</td>
                        <td align="center">25/02/2012</td>
                        <td align="center"><span class="focus">$ 25.12</span></td>
                        <td align="center">PayPal</td>
                        <!--<td align="center">Default Shiping</td>-->
                        <td align="center">Unpaid</td>
                        <td align="center"><div class="opreat"><a href="#" rel="nofollow" class="focus">Pay</a><a href="#" rel="nofollow">Details</a><a href="#" rel="nofollow">Cancel</a></div></td>
                    </tr>--%>
                    <%--<tr>
                        <td colspan="6">
                            <div class="table_vessel01">
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="table_st01">
                                <tbody>
                                    <tr>
                                        <td align="center" style="height: 0px; padding: 0px; border-bottom: none;"></td>
                                        <td width="10%" align="center" style="height: 0px; padding: 0px; border-bottom: none;"></td>
                                        <td width="16%" align="center" style="height: 0px; padding: 0px; border-bottom: none;"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="pro_pic"><a href="#"><img width="45" height="45" src="/uploadfiles/co_pros/2011_12/7dbcb1253fa.jpg" alt="" /></a></div>
                                            <div class="pro_txt">
                                                <h2 class="ti"><a href="#">Sowill oioi s7 Cortex A8 800MHZ - Capacitive Touchscreen 7 Inch 1024X600 Tablet Android 2.2 Universiade 2011 </a></h2>
                                                <p class="remark"></p>
                                            </div>
                                        </td>
                                        <td align="center">8</td>
                                        <td align="center" class="last"><span class="result">US $1696.68</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="pro_pic"><a href="#"><img width="45" height="45" src="/uploadfiles/co_pros/2011_12/7dbcb1253fa.jpg" alt="" /></a></div>
                                            <div class="pro_txt">
                                                <h2 class="ti"><a href="#">Sowill oioi s7 Cortex A8 800MHZ - Capacitive Touchscreen 7 Inch 1024X600 Tablet Android 2.2 Universiade 2011 </a></h2>
                                                <p class="remark">store:16G color:black </p>
                                            </div>
                                        </td>
                                        <td align="center">8</td>
                                        <td align="center" class="last"><span class="result">US $1696.68</span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="pro_pic"><a href="#"><img width="45" height="45" src="/uploadfiles/co_pros/2011_12/7dbcb1253fa.jpg" alt="" /></a></div>
                                            <div class="pro_txt">
                                                <h2 class="ti"><a href="#">Sowill oioi s7 Cortex A8 800MHZ - Capacitive Touchscreen 7 Inch 1024X600 Tablet Android 2.2 Universiade 2011 </a></h2>
                                                <p class="remark"></p>
                                            </div>
                                        </td>
                                        <td align="center">8</td>
                                        <td align="center" class="last"><span class="result">US $1696.68</span></td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>--%>
                    <asp:Repeater id="Ft_List" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td align="center"><%#Eval("orderno")%></td>
                                <td align="center"><%#Eval("stime")%></td>
                                <td align="center"><span class="focus"><%#Eval("subtotal")%></span></td>
                                <td align="center"><%#Eval("paymeth")%></td>
                                <%--<td align="center">Default Shiping</td>--%>
                                <td align="center"><%#Eval("status")%></td>
                                <td align="center"><div class="opreat"><a href="#" rel="nofollow" class="focus">Pay</a><a href="#" rel="nofollow">Details</a><a href="#" rel="nofollow">Cancel</a></div></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <%--<tr>
                        <td align="center">945737748514</td>
                        <td align="center">25/02/2012</td>
                        <td align="center"><span class="focus">$ 25.12</span></td>
                        <td align="center">PayPal</td>
                        <!--<td align="center">Default Shiping</td>-->
                        <td align="center">Unpaid</td>
                        <td align="center"><div class="opreat"><a href="#" rel="nofollow" class="focus">Pay</a><a href="#" rel="nofollow">Details</a><a href="#" rel="nofollow">Cancel</a></div></td>
                    </tr>
                    <tr>
                        <td align="center">945737748514</td>
                        <td align="center">25/02/2012</td>
                        <td align="center"><span class="focus">$ 25.12</span></td>
                        <td align="center">PayPal</td>
                        <!--<td align="center">Default Shiping</td>-->
                        <td align="center">Unpaid</td>
                        <td align="center"><div class="opreat"><a href="#" rel="nofollow" class="focus">Pay</a><a href="#" rel="nofollow">Details</a><a href="#" rel="nofollow">Cancel</a></div></td>
                    </tr>--%>
                </tbody>
                </table>
            </div>
        </div>
    </div><!--RightDiv-->
    
	<Stencil:AccLeftTemp id="AccLeftTemp_" runat="server" />
    
    <p class="mclear"></p>
    
</div><!--UserCen-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</body>
</html>