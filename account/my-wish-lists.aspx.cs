﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;

public partial class account_my_wish_lists : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();

        string uid = string.Empty;
        try { uid = Session["MemberID"].ToString(); }
        catch { Response.Redirect("/signin/"); }

        FCtrl xFctrl = new FCtrl();
        string[] aRate = xFctrl.Cvrate();
        double nPrice = 0;
        double nResult = 0;
        DataTable Dtab = new DataTable();

        try
        {
            SqlDataReader objUpData_DelMark = Tc.ConnDate("delete " + TableName.db_wishlist + " where delmark=1");
            objUpData_DelMark.Close();
            objUpData_DelMark.Dispose();

            Dtab = Tc.ConnDate("select atbm.id,atbm.memid,atbm.proid,atbm.amount,atbm.nprice,atbm.remark,atbm.relea_time,atbn.title,atbn.price,atbn.small_pic,atbn.static,'-' xprice,'-' xresult from " + TableName.db_wishlist + " as atbm," + TableName.db_product + " as atbn where memid=" + uid + " and atbm.proid=atbn.id order by atbm.relea_time desc", 0);

            for (int i = 0; i < Dtab.Rows.Count; i++)
            {
                Dtab.Rows[i]["remark"] = Dtab.Rows[i]["remark"].ToString().Replace("₪", " ");

                if (Dtab.Rows[i]["nprice"].ToString() == "0")
                {
                    nPrice = Math.Round(Convert.ToDouble(Dtab.Rows[i]["price"]) / Convert.ToDouble(aRate[0]), 2);
                    Dtab.Rows[i]["xprice"] = aRate[1] + nPrice.ToString();
                    //nResult = nPrice * Convert.ToInt16(Dtab.Rows[i]["amount"]);
                    //Dtab.Rows[i]["xresult"] = aRate[1] + nResult.ToString();
                }
                else
                {
                    nPrice = Math.Round((Convert.ToDouble(Dtab.Rows[i]["price"]) + Convert.ToDouble(Dtab.Rows[i]["nprice"])) / Convert.ToDouble(aRate[0]), 2);
                    Dtab.Rows[i]["xprice"] = aRate[1] + nPrice.ToString();
                    //nResult = nPrice * Convert.ToInt16(Dtab.Rows[i]["amount"]);
                    //Dtab.Rows[i]["xresult"] = aRate[1] + nResult.ToString();
                }
            }
            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = Dtab.DefaultView;

            Ft_List.DataSource = objPds;
            Ft_List.DataBind();
			
			Ft_PageTitle.Text = "My Account-My Wish Lists, OfferTablets.com";
        }
        catch
        {
            
        }
        Tc.CloseConn();
    }
}
