﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="confirm-email-address.aspx.cs" Inherits="account_confirm_email_address" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<form id="xform" runat="server" action="">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="UserCen">
	<div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Account</a></div>
	<Stencil:AccLeftTemp id="AccLeftTemp_" runat="server" />
    <div class="RightDiv">
        <div class="acc_box">
            <div class="confirm_email">
              <div class="notice">To make sure your register Email is entered correctly, please confirm your Email address. After successful Email confirmation, 59 free Points will be automatically sent to your account. </div>
              <div class="vessel">
                  <div class="v1">
                    <input type="text" value="dabpop139@163.com" readonly="readonly" class="inputs" name="email" id="emali"/>
                    <input type="submit" value="Send Confirm Email" class="inputs_btn_st01"/>
                  </div>
                  <p class="v2 tips">Click the "Send Confirm Email" button and we will sent a confirmation email to the above email address.</p>
                  <ul class="v3">
                    <li><a href="#" class="">Fail to receive Confirm Email? Click here to look for help.</a></li>
                    <li><a href="#" class="">Want to change your register Email? Click here.</a></li>
                  </ul>
              </div>
            </div>
        </div>
    </div><!--RightDiv-->
    <p class="mclear"></p>
    
</div><!--UserCen-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
<asp:Literal id="Ft_Lalert" runat="server"/>
</form>
</body>
</html>